#!/usr/bin/python

import simplekml
from sys import argv, exit

if(len(argv) < 5):
    print "Usage: %s <tagid> <friendly name> <lat> <long>" % argv[0]
    exit()

tagid = argv[1]
friendlyname = argv[2]
lat = argv[3]
lon = argv[4]

kml = simplekml.Kml()
kml.newpoint(name=friendlyname, coords=[(lon, lat)])
kml.save("%s.kml" % tagid)

