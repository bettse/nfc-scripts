#!/usr/bin/python

import sys
import json
import requests
from os import system
from os import path
import ConfigParser

if(len(sys.argv) < 3):
    sys.exit()

tagid = sys.argv[1]
dir = sys.argv[2]
if(dir == 'OUT'):
    sys.exit()

configfile = tagid + '.cfg'

if not path.isfile(configfile):
    sys.exit()

config = ConfigParser.RawConfigParser()
config.read(configfile)
api = config.get('darksky', 'api')
lat = config.get('darksky', 'lat')
lon = config.get('darksky', 'lon')

url = "https://api.darkskyapp.com/v1/forecast/%s/%s,%s" % (api, lat, lon)
r = requests.get(url)
data = json.loads(r.content)
text = "It is currently %s.  Over the next hour, %s. This forecast provided by the Dark Sky API." % (data['currentSummary'], data['hourSummary'])
system("say %s" % text)

