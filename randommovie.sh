#!/bin/bash

if [[ $2 = "IN" ]]
then
    [ ! -e /tmp/mplayer-control ] && mkfifo -m 644 /tmp/mplayer-control 
    cd /Volumes/4Big/Movies
    /usr/local/bin/mplayer -slave -input file=/tmp/mplayer-control -fs -shuffle *.{avi,mov,mp4,mpg,mpeg,mkv,wmv,flv} */*.{avi,mov,mp4,mpg,mpeg,mkv,wmv,flv} */*/*.{avi,mov,mp4,mpg,mpeg,mkv,wmv,flv} 2>&1 &> /dev/null
else
    echo "quit" > /tmp/mplayer-control
    [ -e /tmp/mplayer-control ] && rm /tmp/mplayer-control 
fi
