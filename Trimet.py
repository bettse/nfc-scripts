#!/usr/bin/python

import sys
import json
import requests
from os import system
from os import path
import ConfigParser
import xmltodict
from datetime import datetime
import time

if(len(sys.argv) < 3):
    sys.exit()

tagid = sys.argv[1]
dir = sys.argv[2]
if(dir == 'OUT'):
    sys.exit()

configfile = tagid + '.cfg'

if not path.isfile(configfile):
    sys.exit()

config = ConfigParser.RawConfigParser()
config.read(configfile)
stopid = config.get('trimet', 'stopid')
api = config.get('trimet', 'api')

url = "http://developer.trimet.org/ws/V1/arrivals/locIDs/%s/appID/%s" % (stopid, api)
r = requests.get(url)
data = xmltodict.parse(r.content)
text = ""

for arrival in data['resultSet']['arrival']:
    if(arrival.has_key('@route') and arrival.has_key('@estimated')):
        date = datetime.fromtimestamp(float(arrival['@estimated']) / 1000)
        now = datetime.now()
        diff = int((date - now).total_seconds() / 60)
        if (diff > 1 and diff < 30):
            text += "%s arrives in %s min. " % (arrival['@route'], diff)

system("say %s" % text)


